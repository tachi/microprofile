Origin: upstream, https://github.com/jonasmr/microprofile/pull/75/commits/0cc3b99a134934c3d230112a0b1fe0c6967a28d7
Last-Update: 2021-12-05

From 7f97ffb24fe71062cf32b7a2969f917d08dbc0b2 Mon Sep 17 00:00:00 2001
From: Andrea Pappacoda <andrea@pappacoda.it>
Date: Mon, 29 Nov 2021 18:35:01 +0100
Subject: [PATCH] build: add Meson support

I was trying to package microprofile for Debian, and I had to add a
pkg-config file generator to the CMake script so that the library would
have been easily discoverable. After a bit of trials, I gave up and
decided to write a Meson script instead, since it has a built-in
pkg-config generator.

This build file can (obviously) compile the simple library just like the
CMake one, but it also provides a rich install target that is really
useful to downstream Linux distributions, and makes it also easier for
Meson users to use microprofile as a subproject.

If you are concerned with maintainability, don't worry. As you can
see there's nothing too complex, and unless you change the layout of the
.cpp and .h files, this file should not need any modification. In any
case, if some issues arise, you can always @ ping me :)

The reason why I'm upstreaming this instead of just maintaining it as a
patch in the Debian package is to simplify packaging and reduce
fragmentation - if every distro packages this library differently, its
users will have to account for x different ways of accessing the system
library, and it is not great :/
---
 meson.build       | 78 +++++++++++++++++++++++++++++++++++++++++++++++
 meson_options.txt |  1 +
 2 files changed, 79 insertions(+)
 create mode 100644 meson.build
 create mode 100644 meson_options.txt

diff --git a/meson.build b/meson.build
new file mode 100644
index 0000000..02c1641
--- /dev/null
+++ b/meson.build
@@ -0,0 +1,78 @@
+# SPDX-FileCopyrightText: 2021 Andrea Pappacoda <andrea@pappacoda.it>
+#
+# SPDX-License-Identifier: MIT
+
+project(
+	'microprofile',
+	'cpp',
+	default_options: [
+		'warning_level=3',
+		'b_lto=true',
+		'b_ndebug=if-release',
+		'cpp_std=c++11'
+	],
+	license: 'MIT',
+	meson_version: '>=0.46.0'
+)
+
+# Dependencies
+
+deps = [dependency('threads')]
+
+extra_args = []
+
+stb_dep = dependency('stb', required: false)
+if stb_dep.found()
+	deps += stb_dep
+	extra_args += '-DMICROPROFILE_SYSTEM_STB'
+endif
+
+if host_machine.system() == 'windows'
+	deps += meson.get_compiler('cpp').find_library('ws2_32')
+endif
+
+include_dir = include_directories('.')
+
+# Custom config
+
+if get_option('microprofile_use_config')
+	extra_args += '-DMICROPROFILE_USE_CONFIG'
+	install_headers('microprofile.config.h')
+endif
+
+# Main build target
+
+libmicroprofile = library(
+	'microprofile',
+	'microprofile.cpp',
+	cpp_args: extra_args,
+	dependencies: deps,
+	include_directories: include_dir,
+	install: true
+)
+
+# Headers and pkg-config installation
+
+install_headers(
+	'microprofile.h',
+	'microprofile_html.h'
+)
+
+import('pkgconfig').generate(
+	libmicroprofile,
+	description: 'microprofile is an embeddable profiler',
+	extra_cflags: extra_args,
+	url: 'https://github.com/jonasmr/microprofile'
+)
+
+# Declaring the dependency so that microprofile can be used as a subproject
+
+microprofile_dep = declare_dependency(
+	compile_args: extra_args,
+	include_directories: include_dir,
+	link_with: libmicroprofile
+)
+
+if meson.version().version_compare('>=0.54.0')
+	meson.override_dependency('microprofile', microprofile_dep)
+endif
diff --git a/meson_options.txt b/meson_options.txt
new file mode 100644
index 0000000..3363ed3
--- /dev/null
+++ b/meson_options.txt
@@ -0,0 +1 @@
+option('microprofile_use_config', type: 'boolean', value: false, description: 'Use custom microprofile.config.h')
